
# Junior Laravel Test Repository

Repository ini ditujukan sebagai test untuk PT Responic


## Setup

Clone Repository ini

```bash
  git clone https://gitlab.com/WAN-69/test-pt-responic.git
```

Masuk ke folder Repository

```bash
  cd test-pt-responic
```

Install dependencies

```bash
  composer install
  npm install / yarn install
```

Generate Key Laravel

```bash
  php artisan generate:key
```

Migrate database dan seed data

```bash
  php artisan migrate --seed
```

Jalankan server

```bash
  php artisan serve
  npm run dev/yarn dev
```

